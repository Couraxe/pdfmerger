#include "imageconverter.h"
#include <QMimeType>
#include <QMimeDatabase>
#include <QTemporaryFile>
#include <QFileInfo>
#include <QDir>
#include <QProcess>
#include <QDebug>

ImageConverter::ImageConverter()
{
}

QString ImageConverter::convert(QString original)
{
    ImageConverter converter;
    return converter.convertToTemp(original);
}

void ImageConverter::convert(QStringList *originals) {

    ImageConverter converter;
    int size = originals->size();
    for(int count = 0; count < size; count++) {
        QString tempFile = converter.convertToTemp(originals->at(count));
        originals->replace(count, tempFile);
    }

}

QString ImageConverter::convertToTemp(QString original)
{

    QMimeDatabase mimeDb;
    QMimeType pdf = mimeDb.mimeTypeForFile("any.pdf", QMimeDatabase::MatchMode::MatchExtension);
    QMimeType mimetype = mimeDb.mimeTypeForFile(original);

    if(mimetype == pdf) {
        return original;
    }

    QFileInfo info(original);
    QString tempFile = QDir::tempPath() + QDir::separator() + info.fileName() + ".pdf";

    return runConvert(original, tempFile) ? tempFile : original;
}


bool ImageConverter::runConvert(QString source, QString destination)
{
    QProcess convertExec;
    QStringList args;
    args.append(source);
    args.append(destination);

    return convertExec.execute("convert", args) == 0;
}

