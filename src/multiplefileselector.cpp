#include "multiplefileselector.h"

#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QPushButton>
#include <QApplication>
#include <QStyle>
#include <QFileDialog>
#include <QFileInfo>
#include <QMimeData>
#include <QDragEnterEvent>
#include <QDebug>

MultipleFileSelector::MultipleFileSelector(QWidget *parent)
    : QWidget(parent)
{
    setup();
    setAcceptDrops(true);
}


MultipleFileSelector::~MultipleFileSelector()
{
}


QList<QUrl> MultipleFileSelector::getFilesInOrder() const
{
    QList<QUrl> result;
    foreach(QString file, fileList)
    {
        result.append(QUrl(file));
    }

    return result;
}


void MultipleFileSelector::setLastOpenDirectory(const QString directoryPath)
{
    QUrl test(directoryPath);
    if(!test.isEmpty() && test.isValid())
        lastOpenLocation = directoryPath;
}

QString MultipleFileSelector::getLastOpenLocation() const
{
    return lastOpenLocation;
}


void MultipleFileSelector::setup()
{

    QHBoxLayout *mainLayout = new QHBoxLayout(this);
    setLayout(mainLayout);

    mergeFileList = new QListView(this);
    mainLayout->addWidget(mergeFileList);

    mergeFileModel = new QStringListModel(this);
    mergeFileModel->setStringList(fileList);
    mergeFileList->setModel(mergeFileModel);

    QVBoxLayout *buttonLayout = new QVBoxLayout(this);
    mainLayout->addLayout(buttonLayout);

    QStyle *style = QApplication::style();
    QPushButton *addFile = new QPushButton(QIcon::fromTheme("list-add", style->standardIcon(QStyle::StandardPixmap::SP_DialogOpenButton)), "", this);
    QPushButton *removeFile = new QPushButton(QIcon::fromTheme("list-remove", style->standardIcon(QStyle::StandardPixmap::SP_DialogCloseButton)), "", this);
    QPushButton *moveUp = new QPushButton(QIcon::fromTheme("go-up", style->standardIcon(QStyle::StandardPixmap::SP_ArrowUp)), "", this);
    QPushButton *moveDown = new QPushButton(QIcon::fromTheme("go-down", style->standardIcon(QStyle::StandardPixmap::SP_ArrowDown)), "", this);
    QPushButton *removeAll = new QPushButton(QIcon::fromTheme("edit-clear", style->standardIcon(QStyle::StandardPixmap::SP_DialogResetButton)), "", this);

    addFile->setToolTip(tr("Add files"));
    removeFile->setToolTip(tr("Remove file"));
    moveUp->setToolTip(tr("Move file up"));
    moveDown->setToolTip(tr("Move file down"));
    removeAll->setToolTip(tr("Clear file list"));

    addFile->setMinimumSize(60, 50);
    removeFile->setMinimumSize(60, 50);
    moveUp->setMinimumSize(60, 50);
    moveDown->setMinimumSize(60, 50);
    removeAll->setMinimumSize(60, 50);

    connect(addFile, SIGNAL(clicked()), this, SLOT(addFiles()));
    connect(removeFile, SIGNAL(clicked()), this, SLOT(removeFile()));
    connect(removeAll, SIGNAL(clicked()), this, SLOT(clearList()));
    connect(moveUp, SIGNAL(clicked()), this, SLOT(moveUp()));
    connect(moveDown, SIGNAL(clicked()), this, SLOT(moveDown()));

    buttonLayout->addWidget(moveUp);
    buttonLayout->addWidget(addFile);
    buttonLayout->addWidget(removeFile);
    buttonLayout->addWidget(removeAll);
    buttonLayout->addWidget(moveDown);

    buttonLayout->setAlignment(Qt::AlignVCenter);
    buttonLayout->setSpacing(10);

}


void MultipleFileSelector::addFiles()
{
    QStringList selectedFiles = QFileDialog::getOpenFileNames(this, tr("Open merge documents"), lastOpenLocation, tr("PDF documents (*.pdf)"));
    foreach(QString file, selectedFiles)
    {
        if(!fileList.contains(file))
            fileList.append(file);
    }
    mergeFileModel->setStringList(fileList);

    if(selectedFiles.size() > 0) {
        const QString lastFile = selectedFiles.last();
        lastOpenLocation = lastFile.left(lastFile.lastIndexOf(QDir::separator()));
    }

    emit listChanged(getFilesInOrder());
}

void MultipleFileSelector::addFiles(const QList<QUrl> files)
{
    foreach(QUrl fileUrl, files)
    {
        QString path = fileUrl.path();
        if(!fileList.contains(path))
            fileList.append(path);
    }
    mergeFileModel->setStringList(fileList);

    emit listChanged(getFilesInOrder());
}

QStringList MultipleFileSelector::setList(QStringList fileList)
{
    QStringList skippedItems;

    this->fileList.clear();
    foreach(QString file, fileList)
    {
        QFileInfo fileInfo(file);
        if(fileInfo.exists() && fileInfo.isFile())
            this->fileList.append(file);
        else
            skippedItems.append(file);
    }

    mergeFileModel->setStringList(this->fileList);
    return skippedItems;
}

void MultipleFileSelector::removeFile()
{
    const QString selected = getSelectedItem();
    if(fileList.contains(selected))
    {
        fileList.removeOne(selected);
    }
    mergeFileModel->setStringList(fileList);

    emit listChanged(getFilesInOrder());
}

void MultipleFileSelector::clearList()
{
    fileList.clear();
    mergeFileModel->setStringList(fileList);

    emit listChanged(getFilesInOrder());
}


void MultipleFileSelector::moveUp()
{
    moveSelectedItem(-1);
}

void MultipleFileSelector::moveDown()
{
    moveSelectedItem(1);
}

void MultipleFileSelector::moveSelectedItem(const int direction)
{
    QString selected = getSelectedItem();
    moveItem(&fileList, selected, direction);
    mergeFileModel->setStringList(fileList);

    const int index = fileList.indexOf(selected);
    const QModelIndex modelIndex = mergeFileModel->index(index);
    mergeFileList->selectionModel()->select(modelIndex,QItemSelectionModel::Select);

    emit listChanged(getFilesInOrder());
}

const QString MultipleFileSelector::getSelectedItem() const
{
    const QModelIndexList selected = mergeFileList->selectionModel()->selectedIndexes();
    foreach(const QModelIndex index, selected)
    {
        return mergeFileModel->data(index).toString();
    }

    return "";
}

QStringList *MultipleFileSelector::moveItem(QStringList *source, const QString item, const int direction) const
{
    const int index = source->indexOf(item);
    if(index >= 0 && index + direction >= 0 && index + direction < source->length())
    {
        source->move(index, index + direction);
    }

    return source;
}

void MultipleFileSelector::dragEnterEvent(QDragEnterEvent *event)
{
    if(event->mimeData()->hasUrls())
    {
        foreach(QUrl url, event->mimeData()->urls())
        {
            event->acceptProposedAction();
            return;
        }

    }
}

void MultipleFileSelector::dropEvent(QDropEvent *event)
{
    addFiles(event->mimeData()->urls());
}

bool MultipleFileSelector::isPdfUrl(const QUrl url)
{
    QString file = url.fileName().toLower();
    return file.endsWith(".pdf");
}
