#ifndef SUCCESSHANDLER_H
#define SUCCESSHANDLER_H

#include <QObject>
#include <QWidget>
#include <QStringList>


class SuccessHandler: public QObject
{
    Q_OBJECT

public:
    enum FileManager
    {
        Dolphin,
        Nautilus,
        Explorer,
        Unknown
    };


    static void showSuccessMessage(QWidget *parent, const QString filePath);
    static void showFailureMessage(QWidget *parent, const QString errors);

private:

    SuccessHandler();

    FileManager determineFileManager() const;

    QString runCommand(const QString command, const QStringList arguments) const;
    void showCustomMessageBox(QWidget *parent, const FileManager fileManager, const QString filePath) const;

    static QString messageboxSuccessText();
    static QString messageboxHeadText();

private slots:
    void launchFileManager(const FileManager fileManager, const QString path) const;
    void openFile(const QString filePath) const;

};

#endif // SUCCESSHANDLER_H
